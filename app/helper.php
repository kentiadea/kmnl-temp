<?php

/**
 * Helper function for numbering table data of pagination model
 * 
 * @param  Request $request http request
 * @param  Integer $dataCountToDisplay  number of data display in pagination
 * @return Integer
 */
function numbering_pagination($request, $dataCountToDisplay)
{
	$page = 1;
	if($request->has('page') && $request->get('page') > 1)
	{
		$page += ($request->get('page') - 1) * $dataCountToDisplay;
	}

	return $page;
}

/**
 * Check active state of sidebar menu based on request uri
 * 
 * @param  String $uri uri request
 * @return String
 */
function checkActiveMenu($uri)
{
	$class = '';
	if (Request::is($uri)) {
		$class = 'active';
	}
	return $class;
}

/**
 * Reformat partial date to mysql format date
 * 
 * @param  Array $data date
 * @return String
 */
function reformatBirthDate($data)
{
	$day   = array_pull($data, 'dd');
	$month = array_pull($data, 'mm');
	$year  = array_pull($data, 'yy');
	
	if ($day < 10) {
		$day = '0'.$day;
	}
	
	if ($month < 10) {
		$month = '0'.$month;
	}

	return  $year.'-'.$month.'-'.$day;
}

/**
 * Get partial date with given formatted date
 * 
 * @param  String $date   formatted date
 * @param  string $option partial date option
 * @return String
 */
function getPartialDate($date, $option = 'day')
{
	$dt   = Carbon\Carbon::parse($date);
	$data = $dt->day;
	if (strtolower($option) == 'month') {
		$data = $dt->month;
	} elseif (strtolower($option) == 'year') {
		$data = $dt->year;
	}
	return $data;
}

/**
 * [parseDate description]
 * 
 * @param  [type] $datetime [description]
 * @return [type]           [description]
 */
function parseDate($date, $type = 'date')
{
	$format = 'Y-m-d';
	if ($type == 'datetime') {
		$format = 'Y-m-d H:i:s';
	}
	$dt = Carbon\Carbon::createFromFormat($format, $date);
	if ($type == 'datetime') {
		return $dt->toDayDateTimeString();
	}
	return $dt->toFormattedDateString();
	
}

/**
 * Pagination helper for subfolder problem
 * 
 * @param  String $pagination pagination
 * @return String
 */
function paginationHelper($pagination)
{
	return str_replace('/?', '?', $pagination);
}

/**
 * Change to readable IDR number format
 * @param Integer $value value
 * @return String of new IDR number format
 */
function IDRFormatter($value)
{
	return 'Rp. '.number_format($value, 0, ',', '.');
}

/**
 * Accessible menu using ACL permission
 * 
 * @param  [type]  $treeSection [description]
 * @return boolean              [description]
 */
function isTreeMenuAccessible($treeSection)
{
	$status = false;

	switch ($treeSection) {
		case 'sertifikat':
			if (
				Auth::user()->can('can_read_sertifikatbaru') || 
				Auth::user()->can('can_read_sertifikatperpanjangan')
			) {
				$status = true;
			}
			return $status;
		break;
		case 'report':
			if (
				Auth::user()->can('can_read_laporanpelaksanaan') || 
				Auth::user()->can('can_read_laporanevaluasi') || 
				Auth::user()->can('can_read_laporanasesor') || 
				Auth::user()->can('can_read_laporansebarankompetensi') || 
				Auth::user()->can('can_read_laporanhasilkelulusan') || 
				Auth::user()->can('can_read_laporabiaya')
			) {
				$status = true;
			}
			return $status;
		break;
		default:
			if (
				Auth::user()->can('can_read_masteradministrator') || 
				Auth::user()->can('can_read_masterpeserta') || 
				Auth::user()->can('can_read_masterasesor') || 
				Auth::user()->can('can_read_masterakreditor') || 
				Auth::user()->can('can_read_masterlembagasertifikasi') || 
				Auth::user()->can('can_read_masterbidang') || 
				Auth::user()->can('can_read_masterkompetensi') || 
				Auth::user()->can('can_read_masterunit') || 
				Auth::user()->can('can_read_masterjenjangjabatan') || 
				Auth::user()->can('can_read_masterpendidikan') || 
				Auth::user()->can('can_read_mastergrade') || 
				Auth::user()->can('can_read_masterrole')
			) {
				$status = true;
			}
			return $status;
		break;
	}
}

/**
 * [indonesianDate description]
 * 
 * @param  [type] $date [description]
 * @return [type]       [description]
 */
function indonesianDate($date)
{
	$arrMonth = [
		'01' => 'Januari',
		'02' => 'Februari',
		'03' => 'Maret',
		'04' => 'April',
		'05' => 'Mei',
		'06' => 'Juni',
		'07' => 'Juli',
		'08' => 'Agustus',
		'09' => 'September',
		'10' => 'Oktober',
		'11' => 'November',
		'12' => 'Desember'
	];
	$explode = explode('-', $date);
	return $explode[2].' '.$arrMonth[$explode[1]].' '.$explode[0];
}
