<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'tbl_period';

    /**
     * Alter the automatic "created_at" column name into "period_created_at"
     * 
     * @var string
     */
    const CREATED_AT = "period_created_at";

    /**
     * Alter the automatic "created_at" column name into "period_created_at"
     * 
     * @var string
     */
    const UPDATED_AT = "period_updated_at";

    /**
     * Alter the automatic "created_at" column name into "period_created_at"
     * 
     * @var string
     */
    const DELETED_AT = "period_deleted_at";        

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'period_from_date', 'period_to_date', 'period_status'
    ];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeNoFilter($query)
    {
        return $query;
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('period_status', '=', 1);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where('period_status', '=', 0);
    }

    /**
     * @param $query
     * @param $id
     *
     * @return mixed
     */
    public function scopeWhereId($query, $id)
    {
        return $query->where('period_id', '=', $id);
    }    

}
