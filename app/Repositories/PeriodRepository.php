<?php

namespace App\Repositories;

use App\Entities\Period;

/**
 * Class PeriodRepository
 *
 * @package App\Repositories
 */
class PeriodRepository
{
    /**
     * @var period
     */
    private $model;

    /**
     * PeriodRepository constructor.
     *
     * @param Period $model
     */
    public function __construct(Period $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $pagination
     *
     * @return mixed
     */
    public function all($pagination = 0)
    {
        $collection = $this->model->orderBy('period_created_at', 'desc');
        if ($pagination > 0) {
            return $collection->paginate($pagination);
        }
        return $collection->get()->toArray();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        return $this->model->WhereId($id)->get()->first()->toArray();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function store($data)
    {
         // arrange data
        $persist = [
            'period_from_date' => $data['year_from'].'-'.$data['month_from'].'-'.'01',
            'period_to_date' => $data['year_to'].'-'.$data['month_to'].'-'.'01',
            'period_status' => $data['status'],
            'period_created_by' => 1,
        ];

        return $this->model->create($persist);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function update($id, $data)
    {
        //$period = $this->findById($id);
        //dd($period);
        $period['period_from_date'] = $data['year_from'].'-'.$data['month_from'].'-'.'01';
        $period['period_to_date'] = $data['year_to'].'-'.$data['month_to'].'-'.'01';
        $period['period_status'] = $data['status'];
        $period['period_updated_by'] = '1';

        return $this->model->whereId($id)->update($period);
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $period = $this->model->WhereId($id);
        $period->delete();
    }
}