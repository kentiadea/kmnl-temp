<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/', 'UserAuthController@index');
Route::get('register', 'WelcomeController@register');
Route::get('signin', ['as' => 'user.signin.index', 'uses' => 'UserAuthController@index']);
Route::post('signin', ['as' => 'user.signin.post', 'uses' => 'UserAuthController@signingin']);
Route::get('signout', ['as' => 'user.signout', 'uses' => 'UserAuthController@signingout']);

		
	/**
	*  Routes for master period
	*/
	Route::resource('period', 'PeriodController', ['except' => ['destroy','show']]);
	Route::get('period/{id}/delete', ['as' => 'period.delete', 'uses' => 'PeriodController@destroy']);
