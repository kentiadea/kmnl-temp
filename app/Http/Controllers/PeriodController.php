<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\PeriodFormRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PeriodRepository;

class PeriodController extends Controller
{
    /**
     * @var PeriodRepository
     */
    protected $periodRepository;

    /**
     * @var Request
     */
    protected $httpRequest;

    /**
     * PeriodController constructor.
     *
     * @param PeriodRepository $periodRepository
     * @param Request $httpRequest
     */
    public function __construct(
        PeriodRepository $periodRepository,
        Request $httpRequest
    )
    {
        $this->periodRepository = $periodRepository;
        $this->httpRequest = $httpRequest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->periodRepository->all();
        return view('backend.period.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.period.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|PeriodFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PeriodFormRequest $request)
    {
        if($this->periodRepository->store($request->except('_token'))) {
            //flash()->success('Data has been saved successfully');
            return redirect()->route('period.index');
        }
        $this->httpRequest->flashExcept(['_token']);
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->periodRepository->findById($id);
        return view('backend.period.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|PeriodFormRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(PeriodFormRequest $request, $id)
    {
        if($this->periodRepository->update($id, $request->except(['_token', '_method']))) {
            //flash()->success('Data has been updated successfully');
            return redirect()->route('period.index');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->periodRepository->destroy($id);
        //flash()->success('Data has been deleted');
        return redirect()->route('period.index');
    }
}
