<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/css/bootstrap-select.min.css">
    <!-- Ionicons -->
    <!-- <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('assets/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset('assets/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">


    </head>
    <body>
          <h1>Master Period</h1>
        

    <form class="form-horizontal" action="{{ route('period.update', ['id'=>$data['period_id']]) }}" method="post" accept-charset="utf-8" id="period" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="put"> 
        <div class="form-group">
        <label for="nip" class="col-sm-3 control-label">Dari tanggal</label>
          <div class="col-sm-3">
              {!! Form::selectMonth('month_from', getPartialDate($data['period_from_date'], 'month'),['class'=>'selectpicker form-control','data-live-search' => 'false', 'autocomplete' => 'off']) !!}
          </div>
          <div class="col-sm-2">
              {!! Form::selectRange('year_from', 1990, 2050, getPartialDate($data['period_from_date'], 'year'), ['class'=>'selectpicker form-control','data-live-search' => 'false', 'autocomplete' => 'off']) !!}
          </div>
        </div>
        <br><br><br>
        <div class="form-group">
        <label for="nip" class="col-sm-3 control-label">Sampai tanggal</label>
          <div class="col-sm-3">
              {!! Form::selectMonth('month_to', getPartialDate($data['period_to_date'], 'month'),['class'=>'selectpicker form-control','data-live-search' => 'false', 'autocomplete' => 'off']) !!}
          </div>
          <div class="col-sm-2">
              {!! Form::selectRange('year_to', 1990, 2050, getPartialDate($data['period_to_date'], 'year'), ['class'=>'selectpicker form-control','data-live-search' => 'false', 'autocomplete' => 'off']) !!}
          </div>
        </div>    
        <br><br><br>
         <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tipe</label>
          <div class="col-sm-3">
              {!! Form::select('status', ['1'=>'Aktif','0'=>'Tidak Aktif'], $data['period_status'], ['class'=>'selectpicker form-control', 'id' => $data['period_status'], 'autocomplete' => 'off'])!!}
          </div>
        </div>
        </div>     
        <div class="box-footer col-md-offset-3">
        <button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
        <a href="{{ route('period.index') }}" class="btn btn-warning">Kembali</a>
      </div>
    </form>         
    </body>
        <!-- jQuery 2.1.4 -->
    <script src="{{ asset('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/dist/js/app.min.js') }}" type="text/javascript"></script> 
    <script>
      $('.selectpicker').selectpicker();
      $('.select2').select2({
        placeholder: "Pilih",
        allowClear: true
      });
    </script>
</html>
