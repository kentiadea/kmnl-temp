<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    </head>
    <body>
          <h1>Master Period</h1>
            <div class="box-header with-border">
              <a href="{{ route('period.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Periode</a>
            </div><!-- /.box-header -->

                             <div class="table-responsive">
                    <table class="table no-margin table-condensed table-bordered table-hover table-striped">
                        <thead>
                            <tr class="bg-info">
                              <th class="col-md-2 text-center">From</th>
                              <th class="col-md-2 text-center">To</th>
                              <th class="col-md-1 text-center">Status</th>
                              <th class="col-md-2 text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($data as $period)
                            <tr>
                                <td>{{ $period['period_from_date'] }}</td>
                                <td>{{ $period['period_to_date'] }}</td>
                                <td>{{ ($period['period_status'] == '1') ? 'Aktif' : "Tidak Aktif" }}</td>
                                <td class="text-center">
                                  <a href="{{ route('period.edit', ['id' => $period['period_id']]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Edit</a>
                                  <a href="{{ route('period.delete', ['id' => $period['period_id']]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i>Hapus</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">Tidak ada data</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table><br>
                 </div>


    </body>
</html>
