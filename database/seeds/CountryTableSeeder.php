<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_country')->insert(array(
            array('para_opti_id' => '1', 'para_opti_parameter_id' => '1', 'para_opti_name' => '<= 17'),
            array('para_opti_id' => '2', 'para_opti_parameter_id' => '1', 'para_opti_name' => '18 - 35'),
            array('para_opti_id' => '3', 'para_opti_parameter_id' => '1', 'para_opti_name' => '36 - 53'),
            array('para_opti_id' => '4', 'para_opti_parameter_id' => '1', 'para_opti_name' => '>= 53'),
            array('para_opti_id' => '5', 'para_opti_parameter_id' => '2', 'para_opti_name' => 'Pria'),
            array('para_opti_id' => '6', 'para_opti_parameter_id' => '2', 'para_opti_name' => 'Wanita'),
            array('para_opti_id' => '7', 'para_opti_parameter_id' => '3', 'para_opti_name' => 'Islam'),
            array('para_opti_id' => '8', 'para_opti_parameter_id' => '3', 'para_opti_name' => 'Buddha'),
            array('para_opti_id' => '9', 'para_opti_parameter_id' => '3', 'para_opti_name' => 'Kristen'),
            array('para_opti_id' => '10', 'para_opti_parameter_id' => '3', 'para_opti_name' => 'Katolik'),
            array('para_opti_id' => '11', 'para_opti_parameter_id' => '3', 'para_opti_name' => 'Hindu'),
        )); 
    }
}
