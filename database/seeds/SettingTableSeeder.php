<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_setting')->insert(
			[
			'setting_id' 			=> '1',
			'setting_name'			=> 'Lama Waktu Min. Di ID',
			'setting_description'	=> 'Lama Waktu Min. Di Indonesia (Dlm bulan)',
			'setting_key'			=> 'min-in-indo',
			'setting_value' 		=> '24'	
			]					
		);
    }
}
