<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call('ParameterTableSeeder');
		$this->call('ParameterOptTableSeeder');
		$this->call('PositionTableSeeder');
		$this->call('SettingTableSeeder');
		$this->call('TitleTableSeeder');
    }
}
