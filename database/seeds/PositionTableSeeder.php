<?php

use Illuminate\Database\Seeder;
use Carbon\	Carbon;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tbl_position')->insert(
			[
			'position_id' 			=> '1',
			'position_code' 		=> 'AIP',
			'position_name'			=> 'Atase Ilmu Pengetahuan',
			'position_status'		=> '1',
			'position_created_by' 	=> '1',
			'position_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_position')->insert(
			[
			'position_id' 			=> '2',
			'position_code' 		=> 'AP',
			'position_name'			=> 'Atase Perdagangan',
			'position_status'		=> '1',
			'position_created_by' 	=> '1',
			'position_created_at' 	=> Carbon::now()			
			]				
		);
    }
}
