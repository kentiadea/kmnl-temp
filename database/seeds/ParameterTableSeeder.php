<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\	Carbon;

class ParameterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tbl_parameter')->insert(
			[
			'parameter_id' 			=> '1',
			'parameter_code' 		=> 'AGE',
			'parameter_name'		=> 'Usia',
			'parameter_status'		=> '1',
			'parameter_created_by' 	=> '1',
			'parameter_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_parameter')->insert(
			[
			'parameter_id' 			=> '2',
			'parameter_code' 		=> 'GEN',
			'parameter_name'		=> 'Jenis Kelamin',
			'parameter_status'		=> '1',
			'parameter_created_by' 	=> '1',
			'parameter_created_at' 	=> Carbon::now()			
			]				
		);
		DB::table('tbl_parameter')->insert(
			[
			'parameter_id' 			=> '3',
			'parameter_code' 		=> 'REL',
			'parameter_name'		=> 'Agama',
			'parameter_status'		=> '1',
			'parameter_created_by' 	=> '1',
			'parameter_created_at' 	=> Carbon::now()			
			]					
		);
    }
}
