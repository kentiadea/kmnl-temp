<?php

use Illuminate\Database\Seeder;
use Carbon\	Carbon;

class TitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '1',
			'title_code' 		=> 'PM',
			'title_name'		=> 'Prime Minister',
			'title_level'		=> '1',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '2',
			'title_code' 		=> 'M',
			'title_name'		=> 'Minister',
			'title_level'		=> '2',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '3',
			'title_code' 		=> 'MC',
			'title_name'		=> 'Minister Counsellor',
			'title_level'		=> '3',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '4',
			'title_code' 		=> 'C',
			'title_name'		=> 'Counsellor',
			'title_level'		=> '4',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '5',
			'title_code' 		=> 'S1',
			'title_name'		=> 'Sekretaris I',
			'title_level'		=> '5',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '6',
			'title_code' 		=> 'S2',
			'title_name'		=> 'Sekretaris II',
			'title_level'		=> '6',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '7',
			'title_code' 		=> 'S3',
			'title_name'		=> 'Sekretaris III',
			'title_level'		=> '7',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);		
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '8',
			'title_code' 		=> 'A',
			'title_name'		=> 'Atase',
			'title_level'		=> '8',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '9',
			'title_code' 		=> 'BP',
			'title_name'		=> 'BPKRT',
			'title_level'		=> '9',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);		
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '10',
			'title_code' 		=> 'PK',
			'title_name'		=> 'Petugas Komunikasi',
			'title_level'		=> '10',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);
		DB::table('tbl_title')->insert(
			[
			'title_id' 			=> '11',
			'title_code' 		=> 'SND',
			'title_name'		=> 'Staff Non Diplomatik',
			'title_level'		=> '11',
			'title_status'		=> '1',
			'title_created_by' 	=> '1',
			'title_created_at' 	=> Carbon::now()	
			]					
		);							
    }
}
