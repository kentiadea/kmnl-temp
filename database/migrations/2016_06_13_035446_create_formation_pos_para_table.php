<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationPosParaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_formation_pos_para', function (Blueprint $table) {
            $table->integer('fpp_form_pos_id')->unsigned();
            $table->foreign('fpp_form_pos_id')->references('form_pos_id')->on('tbl_formation_pos');
            $table->index('fpp_form_pos_id');
            $table->integer('fpp_para_opti_id')->unsigned();
            $table->foreign('fpp_para_opti_id')->references('para_opti_id')->on('tbl_parameter_opt');
            $table->index('fpp_para_opti_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_formation_pos_para');
    }
}
