<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_formation', function (Blueprint $table) {
            $table->increments('formation_id');
            $table->string('formation_code', 50);
            $table->integer('formation_country_id')->unsigned();
            $table->foreign('formation_country_id')->references('country_id')->on('tbl_country');
            $table->boolean('formation_status')->default(1)->unsigned();
            $table->index('formation_status');
            $table->dateTime('formation_created_at');
            $table->integer('formation_created_by')->unsigned();
            $table->dateTime('formation_updated_at')->nullable();
            $table->integer('formation_updated_by')->unsigned()->nullable();
            $table->dateTime('formation_deleted_at')->nullable();
            $table->integer('formation_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_formation');
    }
}
