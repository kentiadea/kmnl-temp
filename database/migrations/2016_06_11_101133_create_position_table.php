<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_position', function (Blueprint $table) {
            $table->increments('position_id');
            $table->string('position_code', 50);
            $table->string('position_name', 500)->nullable();
            $table->boolean('position_status')->default(1)->unsigned();
            $table->index('position_status');
            $table->dateTime('position_created_at');
            $table->integer('position_created_by')->unsigned();
            $table->dateTime('position_updated_at')->nullable();
            $table->integer('position_updated_by')->unsigned()->nullable();
            $table->dateTime('position_deleted_at')->nullable();
            $table->integer('position_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_position');
    }
}
