<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_country', function (Blueprint $table) {
            $table->increments('country_id');
            $table->string('country_code', 50);
            $table->string('country_name', 1000)->nullable();
            $table->boolean('country_status')->default(1)->unsigned();
            $table->dateTime('country_created_at');
            $table->integer('country_created_by')->unsigned();
            $table->dateTime('country_updated_at')->nullable();
            $table->integer('country_updated_by')->unsigned()->nullable();
            $table->dateTime('country_deleted_at')->nullable();
            $table->integer('country_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_country');
    }
}
