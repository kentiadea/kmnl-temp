<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterOptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_parameter_opt', function (Blueprint $table) {
            $table->increments('para_opti_id');
            $table->integer('para_opti_parameter_id')->unsigned();
            $table->foreign('para_opti_parameter_id')->references('parameter_id')->on('tbl_parameter');
            $table->string('para_opti_name', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_parameter_opt');
    }
}
