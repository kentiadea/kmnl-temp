<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_setting', function (Blueprint $table) {
            $table->increments('setting_id');
            $table->string('setting_name', 500);
            $table->string('setting_description', 1000)->nullable();
            $table->string('setting_key', 500);
            $table->text('setting_value');
            $table->string('setting_note', 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_setting');
    }
}
