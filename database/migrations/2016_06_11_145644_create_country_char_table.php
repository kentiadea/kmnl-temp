<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryCharTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_country_char', function (Blueprint $table) {
            $table->integer('coun_char_country_id')->unsigned();
            $table->foreign('coun_char_country_id')->references('country_id')->on('tbl_country');
            $table->index('coun_char_country_id');
            $table->integer('coun_char_period_id')->unsigned();
            $table->foreign('coun_char_period_id')->references('period_id')->on('tbl_period');
            $table->index('coun_char_period_id');
            $table->boolean('coun_char_status')->unsigned();
            $table->index('coun_char_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_country_char');
    }
}
