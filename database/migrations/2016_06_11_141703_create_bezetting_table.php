<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBezettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bezetting', function (Blueprint $table) {
            $table->increments('bezetting_id');
            $table->string('bezetting_code', 50);
            $table->integer('bezetting_country_id')->unsigned();
            $table->foreign('bezetting_country_id')->references('country_id')->on('tbl_country');
            $table->integer('bezetting_period_id')->unsigned();
            $table->foreign('bezetting_period_id')->references('period_id')->on('tbl_period');  
            $table->boolean('bezetting_status')->default(1)->unsigned();
            $table->index('bezetting_status');
            $table->dateTime('bezetting_created_at');
            $table->integer('bezetting_created_by')->unsigned();
            $table->dateTime('bezetting_updated_at')->nullable();
            $table->integer('bezetting_updated_by')->unsigned()->nullable();
            $table->dateTime('bezetting_deleted_at')->nullable();
            $table->integer('bezetting_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_bezetting');
    }
}
