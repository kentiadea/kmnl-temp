<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_formation_pos', function (Blueprint $table) {
            $table->increments('form_pos_id');
            $table->integer('form_pos_formation_id')->unsigned();
            $table->foreign('form_pos_formation_id')->references('formation_id')->on('tbl_formation');
            $table->integer('form_pos_position_id')->unsigned();
            $table->foreign('form_pos_position_id')->references('position_id')->on('tbl_position');
            $table->tinyinteger('form_pos_qty')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_formation_pos');
    }
}
