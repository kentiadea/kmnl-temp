<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBezettingPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bezetting_pos', function (Blueprint $table) {
            $table->integer('beze_pos_bezetting_id')->unsigned();
            $table->foreign('beze_pos_bezetting_id')->references('bezetting_id')->on('tbl_bezetting');
            $table->index('beze_pos_bezetting_id');
            $table->integer('beze_pos_position_id')->unsigned();
            $table->foreign('beze_pos_position_id')->references('position_id')->on('tbl_position');
            $table->index('beze_pos_position_id');
            $table->integer('beze_pos_employee_id')->unsigned();
            $table->foreign('beze_pos_employee_id')->references('employee_id')->on('tbl_employee');
            $table->index('beze_pos_employee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_bezetting_pos');
    }
}
