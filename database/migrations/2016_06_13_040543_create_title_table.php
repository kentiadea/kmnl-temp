<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_title', function (Blueprint $table) {
            $table->increments('title_id');
            $table->string('title_code', 50);
            $table->string('title_name', 500)->nullable();
            $table->tinyinteger('title_level')->unsigned();
            $table->boolean('title_status')->default(1)->unsigned();
            $table->index('title_status');
            $table->dateTime('title_created_at');
            $table->integer('title_created_by')->unsigned();
            $table->dateTime('title_updated_at')->nullable();
            $table->integer('title_updated_by')->unsigned()->nullable();
            $table->dateTime('title_deleted_at')->nullable();
            $table->integer('title_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_title');
    }
}
