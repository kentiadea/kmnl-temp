<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_period', function (Blueprint $table) {
            $table->increments('period_id');
            $table->date('period_from_date');
            $table->date('period_to_date');
            $table->boolean('period_status')->default(1)->unsigned();
            $table->index('period_status');
            $table->dateTime('period_created_at');
            $table->integer('period_created_by')->unsigned();
            $table->dateTime('period_updated_at')->nullable();
            $table->integer('period_updated_by')->unsigned()->nullable();
            $table->dateTime('period_deleted_at')->nullable();
            $table->integer('period_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_period');
    }
}
