<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_parameter', function (Blueprint $table) {
            $table->increments('parameter_id');
            $table->string('parameter_code', 50);
            $table->string('parameter_name', 500)->nullable();
            $table->boolean('parameter_status')->default(1)->unsigned();
            $table->index('parameter_status');
            $table->dateTime('parameter_created_at');
            $table->integer('parameter_created_by')->unsigned();
            $table->dateTime('parameter_updated_at')->nullable();
            $table->integer('parameter_updated_by')->unsigned()->nullable();
            $table->dateTime('parameter_deleted_at')->nullable();
            $table->integer('parameter_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_parameter');
    }
}
