<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_application', function (Blueprint $table) {
            $table->increments('application_id');
            $table->integer('application_employee_id')->unsigned();
            $table->foreign('application_employee_id')->references('employee_id')->on('tbl_employee');
            $table->integer('application_country_id')->unsigned();
            $table->foreign('application_country_id')->references('country_id')->on('tbl_country');
            $table->integer('application_period_id')->unsigned();
            $table->foreign('application_period_id')->references('period_id')->on('tbl_period');
            $table->integer('application_position_id')->unsigned();
            $table->foreign('application_position_id')->references('position_id')->on('tbl_position');          
            $table->boolean('application_status')->default(1)->unsigned();
            $table->dateTime('application_created_at');
            $table->integer('application_created_by')->unsigned();
            $table->dateTime('application_updated_at')->nullable();
            $table->integer('application_updated_by')->unsigned()->nullable();
            $table->dateTime('application_deleted_at')->nullable();
            $table->integer('application_deleted_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_application');
    }
}
